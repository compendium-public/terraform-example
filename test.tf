variable "foo" {
  type = string
}

resource "null_resource" "test" {
}

output "test" {
  value = var.foo
}
